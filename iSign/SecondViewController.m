//
//  SecondViewController.m
//  iSign
//
//  Created by Leopoldo G Vargas on 1/27/2014.
//  Copyright (c) 2014 HaiKu. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController
@synthesize key, signature, verLabel, message, DoButton, ReButton, MiButton, FaButton, SolButton, LaButton, SiButton, keyString, keyArray, keyView, keyLabel, messageImg;
-(IBAction)Verify{
    // NSString *OrigMessage = [[NSString alloc]initWithString:message.text];
    //   polinomio llave = [[[NSString alloc]initWithString:key.text]intValue];
    polinomio firma = [[[NSString alloc]initWithString:signature.text]intValue];
    
    FirmasDigitalesUH *signatureVer = [[FirmasDigitalesUH alloc]init];
    
    bool verification = [signatureVer VerificatonAlgorithm:finalKey   :message.text :firma];
    
    if (verification == 1) {
        
        NSString *firmaVER = [NSString stringWithFormat:@"VALID SIGNATURE"];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"VALID SIGNATURE"
                                                        message:@"Your signature has been verified"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

        
        verLabel.text = firmaVER;}
    else {NSString *firmaVER =
        
        [NSString stringWithFormat:@"NOT VALID!!!"];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"NOT VALID!!!"
                                                        message:@"Your signature is not valid for the selected message."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

        verLabel.text = firmaVER;}
    
    
    
    [message resignFirstResponder];
    [signature resignFirstResponder];
    [key resignFirstResponder];
}

- (IBAction) tapBackground: (id) sender{
    [message resignFirstResponder];
    [signature resignFirstResponder];
    [key resignFirstResponder];
    
    if (messgView == 0) {
        
    }else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = message.frame;
        CGRect headerFrame2 = messageImg.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y += 200;
        headerFrame2.origin.y += 200;
        
        message.frame = headerFrame;
        messageImg.frame = headerFrame2;
        
        
        [UIView commitAnimations];
        
        messgView = 0;
    }
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Verify", @"Verify");
        self.tabBarItem.image = [UIImage imageNamed:@"Verify-icono-tab-bar-30x30"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo-640-x-960.png"] drawInRect:(CGRectMake(0, 20, 320, 411))];
            UIImage *imageB = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:imageB];
            
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo iphone5.png"] drawInRect:(CGRectMake(0, 20, 320, 580))];
            UIImage *imageB = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:imageB];
            
        }
    }
    //////
    
    messgView = 0;
    message.delegate =self;
    
	// Do any additional setup after loading the view, typically from a nib.
    // Do any additional setup after loading the view, typically from a nib.
    message.backgroundColor = [UIColor clearColor];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:message.frame] ;
    imageView.image = [UIImage imageNamed:@"Recuadro-texto.png"];
    self.messageImg =imageView;
    [self.view addSubview:imageView];
    [self.view bringSubviewToFront:message];
    ///////
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            UIGraphicsBeginImageContext(self.keyView.frame.size);
            [[UIImage imageNamed:@"Fondo-Key-640-x-960.png"] drawInRect:(CGRectMake(0, 0, 320, 480))];
            UIImage *imagef = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.keyView.backgroundColor = [UIColor colorWithPatternImage:imagef];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            UIGraphicsBeginImageContext(self.keyView.frame.size);
            [[UIImage imageNamed:@"Fondo-key-iphone5.png"] drawInRect:(CGRectMake(0, 0, 320, 580))];
            UIImage *imagef = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.keyView.backgroundColor = [UIColor colorWithPatternImage:imagef];
        }
    }

    ////
    
    keyString = @" ";
    
    keyCount = 0;
    
    finalKey = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)createKEY{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            CGRect headerFrame = keyView.frame;
            headerFrame.origin.y += 590;
            keyView.frame = headerFrame;
            
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            CGRect headerFrame = keyView.frame;
            headerFrame.origin.y += 568;
            keyView.frame = headerFrame;
        }
    }
    
    
    
    
    [UIView commitAnimations];
    
    [self.view bringSubviewToFront:keyView];
    
    
}

-(IBAction)doAct{
    // char first = @"d";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 8231;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"DO" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
 //   AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
}
-(IBAction)reAct{
    //  char first = @"r";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 16441;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    
    
    NSLog(@"%@, %li",  keyArray, f);
    
    //  AudioServices(mySound);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Re" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    
}
-(IBAction)miAct{
    // char first = @"m";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 32785;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Mi" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
 //   AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
}
-(IBAction)faAct{
    // char first = @"f";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 655933;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Fa" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
 //   AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
}
-(IBAction)solAct{
    // char first = @"s";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 131105;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Sol" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    
    
    NSLog(@"%@, %li",  keyArray, f);
}
-(IBAction)laAct{
    //   char first = @"l";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 262221;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"La" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
}
-(IBAction)siAct{
    // char first = @"i";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 524359;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Si" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
}

-(IBAction)DONE{
    if (keyVIEW == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Null Key"
                                                        message:@"Please select a key pattern to sign your messages."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        NSLog(@"Alert");
        
        
    }else{
        PolynomialClass *arithm = [[PolynomialClass alloc]init];
        
        //  polinomio partial = 0;
        
        //  for (int i = 0; i < [keyArray count]; i++) {
        //      polinomio keyP = (polinomio)[keyArray objectAtIndex:i];
        
        //      partial = partial ^ keyP;
        //    NSLog(@"%li", partial); }
        
        keyString = [keyArray componentsJoinedByString: nil];
        
        //NSLog(@"arreglo:%@",keyArray);
        
        //NSLog(@"obatindex:%@",[keyArray objectAtIndex:0]);
        polinomio keyF=(polinomio)0;
        //NSLog(@"preparando llave de long %i",[keyArray count]);
        
        //PROPUESTA PARA GENERAR LLAVE:
        //aquí generamos la llave haciendo xor entre keyF movido 8*i bits y el i-ésimo entero de keyArray
        //i varia en todo la largo de keyArray para soportar claves de long variable.
        int i;
        for (i=0; i<[keyArray count]; i++) {
            keyF=( [[keyArray objectAtIndex:i]intValue])^(keyF<<8);
            //si quitas el comentario siguiente verás cómo se va construyendo la clave
            NSLog(@"llave:%lu, %i",keyF,[[keyArray objectAtIndex:i]intValue]);
        }
        
        
        polinomio keyINT = [arithm residuo:keyF :4295000729];
        
        // keyString = [NSString stringWithFormat:@"%li", keyINT];
        
        finalKey = keyINT;
        
        NSLog(@"Key = %@, %li",  keyString, keyINT);
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = keyView.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y -= 590;
        
        keyView.frame = headerFrame;
        
        keyLabel.text = [NSString stringWithFormat:@"Key = %@, %li",  keyString, keyINT];
        
        
        
        [UIView commitAnimations];
        
        [keyArray removeAllObjects];
        
        keyVIEW = 0;
        
    }
    
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
    
    if (messgView==1) {
        
    }else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = message.frame;
        CGRect headerFrame2 = messageImg.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y -= 200;
        headerFrame2.origin.y -= 200;
        
        message.frame = headerFrame;
        messageImg.frame = headerFrame2;
        
        
        [UIView commitAnimations];
        
        messgView = 1;
    }
    //    message.backgroundColor = [UIColor clearColor];
    //   UIImageView *imageView = [[[UIImageView alloc] initWithFrame:message.frame] autorelease];
    //  imageView.image = [UIImage imageNamed:@"Recuadro-texto.png"];
    //[self.view addSubview:imageView];
    //[self.view bringSubviewToFront:message];
    
    
    //  textView.backgroundColor = [UIColor greenColor];
}




@end


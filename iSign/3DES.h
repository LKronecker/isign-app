//
//  azGeneratePassword.h
//  JM_mobile
//
//  Created by j05u3 on 11/02/14.
//  Copyright (c) 2014 Ascenzo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface azGeneratePassword : NSObject
@property (strong,nonatomic)NSData *passwordData;
@property (strong,nonatomic)NSData *keyData;
@property (strong,nonatomic)NSString *password;
@property (strong,nonatomic)NSString *key;
- (NSData *)tripleDesEncryptData;
- (void)tripleDesKeyCreation;

@end

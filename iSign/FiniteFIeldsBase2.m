//
//  FiniteFIeldsBase2.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"
@implementation FiniteFIeldsBase2

-(NSArray *) generateField: (int)exp{
   
    base =2;
    exponent = exp;
    int ord = pow(base, exp);
    order = ord;
    
    NSMutableArray *tempArray = [[NSMutableArray alloc]init];
    for (int i = 0; i<order; i++) {
        PolynomialClass *gen = [[PolynomialClass alloc]init];
        NSString *temp = nil;
        temp = [gen generatePoly:i];
       // NSString *tempstr = [NSString stringWithFormat:@"%ln", temp];
        [tempArray insertObject:temp atIndex:i];
        [gen release];
       // [temp release];
       // NSLog(@"%@", [tempArray objectAtIndex:i]);
    }
   // elements = tempArray;
    return (tempArray);
    [tempArray release];
}
-(NSArray *)irreductiblePolynomials: (int)exp{
    PolynomialClass *GLfields = [[PolynomialClass alloc]init];
    if (exp == 1) {
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"1", nil];
        irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:1], nil];
        return (irredPolIntValArray);}
    if (exp == 2) {
        irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:7], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"7", nil];
        return (irredPolIntValArray);}
    if (exp == 3) {
        irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:11], [GLfields generatePoly:14], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"11",@"13", nil];
        return (irredPolIntValArray);}
    if (exp == 4) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:19],[GLfields generatePoly:25], nil];
         irredPolIntValArray = [[NSArray alloc]initWithObjects:@"19",@"25", nil];
        return (irredPolIntValArray);}
    if (exp == 5) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:37],[GLfields generatePoly:41],[GLfields generatePoly:47],[GLfields generatePoly:55], nil]; 
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"37",@"41",@"47",@"55", nil];
        return (irredPolIntValArray);}
    if (exp == 6) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:67],[GLfields generatePoly:91],[GLfields generatePoly:97],[GLfields generatePoly:103], nil]; 
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"67",@"91",@"97",@"103", nil];
        return (irredPolIntValArray);}
    if (exp == 7) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:131],[GLfields generatePoly:137],[GLfields generatePoly:143],[GLfields generatePoly:145], nil]; 
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"131",@"137",@"143",@"145", nil];
        return (irredPolIntValArray);}
    if (exp == 8) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:285],[GLfields generatePoly:299],[GLfields generatePoly:301],[GLfields generatePoly:333], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"285",@"299",@"301",@"333", nil];
        return (irredPolIntValArray);}
    if (exp == 9) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:529],[GLfields generatePoly:539],[GLfields generatePoly:545],[GLfields generatePoly:557], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"529",@"539",@"545",@"557", nil];
        return (irredPolIntValArray);}
    
    if (exp == 10) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:1033],[GLfields generatePoly:1051], nil]; 
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"1033",@"1051", nil];
        return (irredPolIntValArray);}
    if (exp == 11) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:2053],[GLfields generatePoly:2071], nil]; 
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"2053",@"2071", nil];
        return (irredPolIntValArray);}
    if (exp == 12) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:4179],[GLfields generatePoly:4201], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"4179",@"4201", nil];
        return (irredPolIntValArray);}
    if (exp == 13) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:8219],[GLfields generatePoly:8231], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"8219",@"8231", nil];
        return (irredPolIntValArray);}
    if (exp == 14) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:16427],[GLfields generatePoly:16441], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"16427",@"16441", nil];
        return (irredPolIntValArray);}
    if (exp == 15) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:32771],[GLfields generatePoly:32785], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"32771",@"32785", nil];
        return (irredPolIntValArray);}
    if (exp == 16) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:65581],[GLfields generatePoly:65593], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"65581",@"655933", nil];
        return (irredPolIntValArray);}
    if (exp == 17) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:131081],[GLfields generatePoly:131105], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"131081",@"131105", nil];
        return (irredPolIntValArray);}
    if (exp == 18) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:262183],[GLfields generatePoly:262221], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"262183",@"262221", nil];
        return (irredPolIntValArray);}
    if (exp == 19) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:524327],[GLfields generatePoly:524359], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"524327",@"524359", nil];
        return (irredPolIntValArray);}
    if (exp == 32) {irreducPolyArray = [[NSArray alloc]initWithObjects:[GLfields generatePoly:4295000729],[GLfields generatePoly:4295000729], nil];
        irredPolIntValArray = [[NSArray alloc]initWithObjects:@"4295000729", nil];
        return (irredPolIntValArray);}




    [GLfields release];
    return 0;
}
-(polinomio)sumaGFbase2:(polinomio)p:(polinomio)q{
    PolynomialClass *sumaPQ = [[PolynomialClass alloc]init];
    polinomio pMASq = p^q;
    NSString *irredString = [irredPolIntValArray objectAtIndex:0];
    polinomio irreducPoly = [irredString intValue];
    
    polinomio pMASqMOD = [sumaPQ residuo:pMASq :irreducPoly];
   // [sumaPQ release];
    return (pMASqMOD);
    [sumaPQ release];

}
-(polinomio)productoGFbase2:(polinomio)p:(polinomio)q{
     PolynomialClass *prodPQ = [[PolynomialClass alloc]init];
  polinomio pPORq = [prodPQ multiplicacion:p :q];
    NSString *irredString = [irredPolIntValArray objectAtIndex:0];
    polinomio irreducPoly = [irredString intValue];
    
    polinomio pPORqMOD = [prodPQ residuo:pPORq :irreducPoly];
       
   // polinomio pPORqMOD = [prodPQ residuo:[prodPQ multiplicacion:pMod :qMod]:irreducPoly ];
    
    [prodPQ release];
    return (pPORqMOD);
}

-(polinomio)elevarGFbase2:(polinomio)p:(int)exp{
   // polinomio irred = [[irredPolIntValArray objectAtIndex:0] intValue];
    PolynomialClass *prodMod = [[PolynomialClass alloc]init];
    if (exp == 0) return (1);
    if (exp == 1) return (p);
    else {
       polinomio p1 = p;
        for (int i = 2; i<exp+1; i++) {
            p1 = [prodMod multiplicacion:p :p1];
        }
        [prodMod release];
        return (p1);
    }

}

-(void) displayField{
    NSLog(@"Soy un Campo de orden %ld con exponente %i y base 2.", order, exponent);
   // PolynomialClass *polIrredu = [[PolynomialClass alloc]init];
   
    
    elements = [self generateField:exponent];
       for (int i = 0; i< order; i++) {
           NSString * elem = [NSString stringWithFormat:@"%i: %@", i, [elements objectAtIndex:i]];
        NSLog(@"%@", elem);
    }
    
    
    

}

-(polinomio) compressionEndomorfism:(polinomio)p:(int)newDimension{
    PolynomialClass *poly = [[PolynomialClass alloc]init];
    NSArray *irred = [self irreductiblePolynomials:newDimension];
    polinomio newIrred = [[irred objectAtIndex:0]intValue];
  //  NSLog(@"%ld", newIrred);
   // NSArray *irred1 = [self irreductiblePolynomials:newDimension-1];
    //polinomio newIrred1 = [[irred1 objectAtIndex:0]intValue];
    //NSLog(@"%ld", newIrred1);
    
    polinomio compre = [poly residuo:p :newIrred];
   //     NSLog(@"%ld", compre);
 //   polinomio comPart = [poly residuo:p :newIrred1];
   //     NSLog(@"%ld", comPart);
  //  polinomio compFinal = compre ^ comPart;
    return (compre);
}

@end

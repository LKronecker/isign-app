//
//  FirmasDigitalesUH.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 2012-11-11.
//
//

#import <Foundation/Foundation.h>
#import "PolynomialRingOverGL(2^n).h"
#import "MessageToPolynomial.h"
#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"

@interface FirmasDigitalesUH : NSObject

-(polinomio)SigningAlgorithm: (polinomio)key: (NSString *)message;
-(BOOL)VerificatonAlgorithm: (polinomio)key: (NSString *)message: (polinomio)signature;

@end

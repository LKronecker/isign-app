//
//  FirstViewController.m
//  iSign
//
//  Created by Leopoldo G Vargas on 1/27/2014.
//  Copyright (c) 2014 HaiKu. All rights reserved.
//

#import "FirstViewController.h"
#import "FirmasDigitalesUH.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
@synthesize key, message, signLabel, imageDictinary, imgPicker, imageData, imageName, imageURL, DoButton, ReButton, MiButton, FaButton, SolButton, LaButton, SiButton, keyString, keyArray, keyView, keyLabel, signButton, messageImg, ChipherState, ORIGINstate, Signature;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Sign", @"Sign");
        self.tabBarItem.image = [UIImage imageNamed:@"Sign-icono-tab-bar-30x30"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    keySelectionCount = 0;
    messgView = 0;
    message.delegate =self;
	// Do any additional setup after loading the view, typically from a nib.
    message.backgroundColor = [UIColor clearColor];
    
    //  UIGraphicsBeginImageContext(self.message.frame.size);
    // [[UIImage imageNamed:@"Recuadro-texto.png"] drawInRect:(message.frame)];
    // UIImage *imageT = UIGraphicsGetImageFromCurrentImageContext();
    // UIGraphicsEndImageContext();
    // message.backgroundColor = [UIColor colorWithPatternImage:imageT];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:message.frame] ;
    imageView.image = [UIImage imageNamed:@"Recuadro-texto.png"];
    self.messageImg =imageView;
    [self.view addSubview:imageView];
    [self.view bringSubviewToFront:message];
    ///
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo-640-x-960.png"] drawInRect:(CGRectMake(0, 20, 320, 411))];
            UIImage *imageB = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:imageB];

            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            UIGraphicsBeginImageContext(self.view.frame.size);
            [[UIImage imageNamed:@"Fondo iphone5.png"] drawInRect:(CGRectMake(0, 20, 320, 580))];
            UIImage *imageB = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            self.view.backgroundColor = [UIColor colorWithPatternImage:imageB];

        }
    }

    
       /////
    
    signButton.enabled = NO;
    
    keyVIEW = 0;
    
    keyString = @" ";
    
    keyCount = 0;
    
    
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            UIGraphicsBeginImageContext(self.keyView.frame.size);
            [[UIImage imageNamed:@"Fondo-Key-640-x-960.png"] drawInRect:(CGRectMake(0, 0, 320, 480))];
            UIImage *imagef = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.keyView.backgroundColor = [UIColor colorWithPatternImage:imagef];
            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            UIGraphicsBeginImageContext(self.keyView.frame.size);
            [[UIImage imageNamed:@"Fondo-key-iphone5.png"] drawInRect:(CGRectMake(0, 0, 320, 580))];
            UIImage *imagef = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            self.keyView.backgroundColor = [UIColor colorWithPatternImage:imagef];
        }
    }

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)Sign{
    
    //  if ([keyString isEqualToString:@" "]) {
    //     UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Wait!!!" message: @"Generate a KEY first!!!" delegate: nil cancelButtonTitle:@"OK" otherButtonTitles:nil]; [alert show];
    //     [alert release];
    //  }else{
    
    
    FirmasDigitalesUH *signature = [[FirmasDigitalesUH alloc]init];
    //    PolynomialClass *arithmetics = [[PolynomialClass alloc]init];
    
    polinomio firma = [signature SigningAlgorithm:finalKey :[[NSString alloc]initWithString:message.text]];
    
    NSString *firmaST = [NSString stringWithFormat:@"%li", firma];
    
    NSLog(@"%li: %@", firma, firmaST);
    
    signLabel.text = firmaST;
    self.Signature = firmaST;
    
    //  [key resignFirstResponder];
    [message resignFirstResponder];
    
    //  }
    
    if (messgView == 0) {
        
    }else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = message.frame;
        CGRect headerFrame2 = messageImg.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y += 200;
        headerFrame2.origin.y += 200;
        
        message.frame = headerFrame;
        messageImg.frame = headerFrame2;
        
        
        [UIView commitAnimations];
        
        messgView = 0;
    }
    
    [self LFSRcipher];
    
    
}

-(IBAction)LFSRcipher{

    lfsrCipher *cipher = [[lfsrCipher alloc]init];
    
    NSString * mesg= self.message.text;
    
  //  NSString * mesg=@"Retomando los esquemas de cifrado";
    
    //NSString * cif = [cipher CipherLFSR:mesg :45];
    
    //NSLog(@"Try:%@", cif ); //Octavio sigue dando lata
    
    //Objective C declara NSString de manera oscura
    //además codifica los símbolos imprimibles en UTF16 (tiene sus ventajas)
    //Tarea: aprender mas de la codificación/decodificación en UTF16
    //por lo pronto...
    //vamos a convertir la "cadena" a bytes (char *): OJO: no todos son imprimibles
    //y tenemos pedos con caracteres no standard, com la ñ, parece que no está en el ascii de ios
    const char * enC=[(NSString *)mesg UTF8String ];
    //y vemos que ya podemos tener acceso a los caracteres,
    //por ejemplo los primeros 3:
    //NSLog(@"%c %c %c",enC[0],enC[1],enC[2]);
    //y ahora toda:
    //NSLog(@"%s",enC);
    //nota que la ñ vale madres
    //acceso via polinomios
    //NSLog(@"enpoli:%u",((polinomio *)enC)[0]);
    //NSLog(@"enpoli:%u",((polinomio *)enC)[1]);
    //NSLog(@"enpoli:%u",((polinomio *)enC)[2]);
    //NSLog(@"enpoli:%u",((polinomio *)enC)[3]);
    ///////
    //Voy a declarar CipherLFSRchar en lfsrCipher.h y .m para no enredarme
    // y lo mando llamar
    //
    // tambien declaro version void
    
    NSUInteger cnt = [(NSString *)mesg length]; //truco para contar bytes?
    NSLog(@"cnt:%ld",(unsigned long)cnt);
    char ciphertext[cnt];
    //  enC=(char *)"Retomando los esquemas de cifrado";
    [cipher CipherLFSRchar: (char *)enC :4: (int)cnt:ciphertext];
    
    //aquí se nota lo que te decía cuando ciframos de polinomio en polinomio
    NSLog(@"CIFRADO: %s",ciphertext);
    
    NSString *ciph = [NSString stringWithFormat:@"%s", ciphertext];
    self.ChipherState = ciph;
    
    
    enC=ciphertext;
    [cipher CipherLFSRchar:(char *)enC :4:(int)cnt:ciphertext];
    NSLog(@"DESCIFRADO: %s",ciphertext);
    
    NSString *or = [NSString stringWithFormat:@"%s", ciphertext];
    self.ORIGINstate = or;
    
    
    int o;
    for (o=0; o<cnt+1; o++) {
     //   NSLog(@"%c",ciphertext[o]);
    }
    //NSLog(@"%@",(NSString *)ciphertext[0]);
  //  exit(0); //octavio
    
    [self alert];

}

-(void)alert{
    
    NSString *data = [NSString stringWithFormat:@"%@ SIGNATURE: %@", ChipherState, Signature];

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cyphertext & Signature"
                                                    message:data
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];


}

-(IBAction)SignImage{
    
    polinomio llave = [[[NSString alloc]initWithString:key.text]intValue];
    FirmasDigitalesUH *signature = [[FirmasDigitalesUH alloc]init];
    PolynomialClass *arithmetics = [[PolynomialClass alloc]init];
    
    // NSString* content = [NSString stringWithContentsOfFile:imageURL
    //         encoding:NSUTF8StringEncoding
    //          error:NULL];
    //  NSData *fileData = [NSData dataWithContentsOfURL:imageURL];
    NSString *fileContents = [NSString stringWithContentsOfURL:imageURL encoding:NSUTF8StringEncoding error:nil];
    // NSData *fileData = [NSData dataWithContentsOfFile:[fileContents path]];
    NSLog(@"%@", fileContents);
    
    // NSString *imageString = [[NSString alloc]initWithData:imageData encoding:NSASCIIStringEncoding];
    
    //  NSLog(@"Image string: %@", imageString);
    
    //   polinomio firma = [signature SigningAlgorithm:llave :imageString];
    
    //  NSString *firmaST = [NSString stringWithFormat:@"%li:%@", firma, [arithmetics generatePoly:firma]];
    
    //  NSLog(@"%li: %@", firma, firmaST);
    
    //  signLabel.text = firmaST;
    
    [key resignFirstResponder];
    [message resignFirstResponder];
    
}

- (IBAction) tapBackground: (id) sender{
    [key resignFirstResponder];
    [message resignFirstResponder];
    
    if (messgView == 0) {
        
    }else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = message.frame;
        CGRect headerFrame2 = messageImg.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y += 200;
        headerFrame2.origin.y += 200;
        
        message.frame = headerFrame;
        messageImg.frame = headerFrame2;
        
        
        [UIView commitAnimations];
        
        messgView = 0;
    }
}

- (IBAction)grabImage {
    imgPicker = [[UIImagePickerController alloc] init];
    
    imgPicker.delegate = self;
    
    
    [self presentViewController:imgPicker animated:YES completion:nil];
    
    
    
    /*    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
     
     {
     
     imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
     
     } else
     
     {
     
     imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
     
     }
     
     //[self presentModalViewController:imgPicker animated:YES];
     
     [self presentViewController:imgPicker animated:YES completion:nil]; */
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
    
    // [[Picker parentViewController] dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
  //  [Picker release];
    
}
- (void)imagePickerController:(UIImagePickerController *) Picker

didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    image.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSData *imageD = UIImageJPEGRepresentation(image.image, 90);
    imageData = imageD;
    
    
    NSURL *imageP = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
    self.imageURL = imageP;
    
    NSString *imageN = [imageP lastPathComponent];
    self.imageName = imageN;
    
    NSLog(@"%@, %@", imageP, imageN);
    
    // [[Picker parentViewController] dismissModalViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // [Picker release];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)img editingInfo:(NSDictionary *)editInfo {
	image.image = img;
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)createKEY{
    [UIView beginAnimations:@"advancedAnimations" context:nil];
	[UIView setAnimationDuration:0.6];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        // The iOS device = iPhone or iPod Touch
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if (iOSDeviceScreenSize.height == 480){
            // iPhone 3/4x
            
            CGRect headerFrame = keyView.frame;
            headerFrame.origin.y += 590;
            keyView.frame = headerFrame;

            
        }else if (iOSDeviceScreenSize.height == 568){
            // iPhone 5 etc
            
            CGRect headerFrame = keyView.frame;
            headerFrame.origin.y += 568;
            keyView.frame = headerFrame;
        }
    }

    
    
    
    [UIView commitAnimations];
    
    signButton.enabled = YES;
    
    [self.view bringSubviewToFront:keyView];
    
    
}

-(IBAction)doAct{
    // char first = @"d";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 8231;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"DO" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
   // AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
    AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount + 1;
    
}
-(IBAction)reAct{
    //  char first = @"r";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 16441;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    
    
    NSLog(@"%@, %li",  keyArray, f);
    
    //  AudioServices(mySound);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Re" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount + 1;
    
}
-(IBAction)miAct{
    // char first = @"m";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 32785;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Mi" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
   // AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount+1;
    
}
-(IBAction)faAct{
    // char first = @"f";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 655933;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Fa" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount+1;
    
}
-(IBAction)solAct{
    // char first = @"s";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 131105;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Sol" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
 //   AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    
    
    NSLog(@"%@, %li",  keyArray, f);
    
    keySelectionCount = keySelectionCount+1;
}
-(IBAction)laAct{
    //   char first = @"l";
    // int f = (int) first;
    keyVIEW = 1;
    polinomio f = 262221;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"La" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
   // AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount+1;
    
}
-(IBAction)siAct{
    // char first = @"i";
    //int f = (int) first;
    keyVIEW = 1;
    polinomio f = 524359;
    NSString * part = [NSString stringWithFormat:@"%li", f];
    NSMutableArray *keyP = [[NSMutableArray alloc]initWithArray:keyArray];
    
    [keyP insertObject:part atIndex:keyCount];
    keyCount = keyCount++;
    
    self.keyArray = keyP;
    
    NSLog(@"%@, %li",  keyArray, f);
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Si" ofType:@"mp3"];
    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
    
  //  AudioServicesCreateSystemSoundID(( CFURLRef)(newURL), &(mySound));
    
     AudioServicesCreateSystemSoundID ((__bridge CFURLRef) newURL, &mySound);
    
    AudioServicesPlaySystemSound(mySound);
    
    keySelectionCount = keySelectionCount+1;
    
    
    
}

-(IBAction)DONE{
    
    NSLog(@"%i", keySelectionCount);
    if (keyVIEW == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Null Key"
                                                        message:@"Please select a key pattern to sign your messages."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        NSLog(@"Alert");
        
        
    }else{
        PolynomialClass *arithm = [[PolynomialClass alloc]init];
        
        //  polinomio partial = 0;
        
        //  for (int i = 0; i < [keyArray count]; i++) {
        //      polinomio keyP = (polinomio)[keyArray objectAtIndex:i];
        
        //      partial = partial ^ keyP;
        //    NSLog(@"%li", partial); }
        
        keyString = [keyArray componentsJoinedByString: nil];
        
        //NSLog(@"arreglo:%@",keyArray);
        
        //NSLog(@"obatindex:%@",[keyArray objectAtIndex:0]);
        polinomio keyF=(polinomio)0;
        //NSLog(@"preparando llave de long %i",[keyArray count]);
        
        //PROPUESTA PARA GENERAR LLAVE:
        //aquí generamos la llave haciendo xor entre keyF movido 8*i bits y el i-ésimo entero de keyArray
        //i varia en todo la largo de keyArray para soportar claves de long variable.
        int i;
        for (i=0; i<[keyArray count]; i++) {
            keyF=( [[keyArray objectAtIndex:i]intValue])^(keyF<<8);
            //si quitas el comentario siguiente verás cómo se va construyendo la clave
            NSLog(@"llave:%lu, %i",keyF,[[keyArray objectAtIndex:i]intValue]);
        }
        
        
        polinomio keyINT = [arithm residuo:keyF :4295000729];
        
        // keyString = [NSString stringWithFormat:@"%li", keyINT];
        
        finalKey = keyINT;
        
        NSLog(@"Key = %@, %li",  keyString, keyINT);
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = keyView.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y -= 590;
        
        keyView.frame = headerFrame;
        
        
        [UIView commitAnimations];
        keyLabel.text = [NSString stringWithFormat:@"Key = %@, %li",  keyString, keyINT];
        
        
        
        [keyArray removeAllObjects];
        
        keyVIEW = 0;
        
    }
    
    //
    NSLog(@"key cont %i", keySelectionCount);
    
    keySelectionCount = 0;
    
}

/*-(BOOL)textViewShouldReturn:(UITextField*)textField;
 {
 NSLog(@"Works");
 
 return YES;
 }  */

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    return YES;
}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
    
    if (messgView==1) {
        
    }else{
        
        [UIView beginAnimations:@"advancedAnimations" context:nil];
        [UIView setAnimationDuration:0.6];
        
        CGRect headerFrame = message.frame;
        CGRect headerFrame2 = messageImg.frame;
        //ventaFrame.origin.y -= 150;
        headerFrame.origin.y -= 200;
        headerFrame2.origin.y -= 200;
        
        message.frame = headerFrame;
        messageImg.frame = headerFrame2;
        
        
        [UIView commitAnimations];
        
        messgView = 1;
    }
    //    message.backgroundColor = [UIColor clearColor];
    //   UIImageView *imageView = [[[UIImageView alloc] initWithFrame:message.frame] autorelease];
    //  imageView.image = [UIImage imageNamed:@"Recuadro-texto.png"];
    //[self.view addSubview:imageView];
    //[self.view bringSubviewToFront:message];
    
    
    //  textView.backgroundColor = [UIColor greenColor];
}



@end


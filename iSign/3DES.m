//
//  azGeneratePassword.m
//  JM_mobile
//
//  Created by j05u3 on 11/02/14.
//  Copyright (c) 2014 Ascenzo. All rights reserved.
//

#import "3DES.h"
#import <CommonCrypto/CommonKeyDerivation.h>
#import <CommonCrypto/CommonCryptor.h>


@implementation azGeneratePassword
- (NSData *)tripleDesEncryptData {
    NSParameterAssert(self.passwordData);
    NSParameterAssert(self.keyData);
    NSError *error;
    size_t outLength;
    
    NSAssert(self.keyData.length == kCCKeySize3DES, @"the keyData is an invalid size");
    
    NSMutableData *outputData = [NSMutableData dataWithLength:(self.passwordData.length  +  kCCBlockSize3DES)];
    
    CCCryptorStatus
    result = CCCrypt(kCCEncrypt, // operation
                     kCCAlgorithm3DES, // Algorithm
                     0, // options
                     self.keyData.bytes, // key
                     self.keyData.length, // keylength
                     nil,// iv
                     self.passwordData.bytes, // dataIn
                     self.passwordData.length, // dataInLength,
                     outputData.mutableBytes, // dataOut
                     outputData.length, // dataOutAvailable
                     &outLength); // dataOutMoved
    
    if (result != kCCSuccess) {
        if (error != NULL) {
           error = [NSError errorWithDomain:@"com.your_domain.your_project_name.your_class_name."
                                         code:result
                                     userInfo:nil];
        }
        return nil;
    }
    [outputData setLength:outLength];
    return outputData;
}

- (NSData *)derivedKeyFromPassword:(NSString*)password salt:(NSData*)saltData length:(NSUInteger)keyLength
{
    NSMutableData *derivedKey = [NSMutableData dataWithLength:keyLength];
    int result = CCKeyDerivationPBKDF(kCCAlgorithm3DES,                          // algorithm
                                      password.UTF8String,                // password
                                      password.length,                    // passwordLength
                                      saltData.bytes,                     // salt
                                      saltData.length,                    // saltLen
                                      kCCPRFHmacAlgSHA256,                // PRF
                                      10000,                              // rounds
                                      derivedKey.mutableBytes,            // derivedKey
                                      keyLength);                         // derivedKeyLen
    
    if (result != kCCSuccess) {
        NSError *error = [NSError errorWithDomain:@"com.your_domain.your_project_name.your_class_name" code:result userInfo:nil];
        NSLog(@"error: %@", error);
        return nil;
    }
    return derivedKey;
}

- (NSData *)randomDataOfLength:(size_t)length
{
    NSMutableData *data = [NSMutableData dataWithLength:length];
    int result = SecRandomCopyBytes(kSecRandomDefault, length, data.mutableBytes);
    if (result != 0) return nil;
    return data;
}

- (void)tripleDesKeyCreation
{
  /*  Este metodo es el que adapta la llave que vamos a usar para cifrar a una representacion adecuada para 3DES, aqui tenemos que meter la llave que nos proporciona el administrador no el password que queremos cifrar.
   La llave proporcionada por el administrador sera convertida a una representacion adecuada para ser utilizada por el esquema de cifrado 3DES. En otras palabraslo que escupa este metodo es lo que vamos a usar para cifrar el "password" que nos da el usuario.
   
    */
    
    NSString *password = self.password;  // aqui necesitamos poner la llave que nos da el admin
    NSUInteger keyLength = kCCKeySize3DES;
    NSUInteger saltLength = 8;
    
    NSData *saltData = [self randomDataOfLength:saltLength];
    
    NSData *derivedKey = [self derivedKeyFromPassword:password salt:saltData length:keyLength]; // esta es la llave que vamos a usar para cifrar.
    NSLog(@"Key:  %@",derivedKey);
}
@end

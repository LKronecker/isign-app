//
//  SecondViewController.h
//  iSign
//
//  Created by Leopoldo G Vargas on 1/27/2014.
//  Copyright (c) 2014 HaiKu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FirmasDigitalesUH.h"
#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"
#import <AudioToolbox/AudioToolbox.h>

@interface SecondViewController : UIViewController<UITextViewDelegate>{
    SystemSoundID mySound;
    
    UIImageView *messageImg;
    bool messgView;
    UIView *keyView;
    NSMutableString *keyString;
    NSMutableArray *keyArray;
    int keyCount;
    bool keyVIEW;
    
    UIButton * DoButton;
    UIButton * ReButton;
    UIButton * MiButton;
    UIButton * FaButton;
    UIButton * SolButton;
    UIButton * LaButton;
    UIButton * SiButton;
    
    UITextField *key;
    UITextField *signature;
    UITextView *message;
    UILabel *verLabel;
    UILabel *keyLabel;
    
    polinomio finalKey;
}
@property(nonatomic, retain) IBOutlet  UILabel *keyLabel;
@property(nonatomic, retain)  NSMutableString *keyString;
@property(nonatomic, retain)  NSMutableArray *keyArray;
@property(nonatomic, retain) IBOutlet   UIView *keyView;
@property(nonatomic, retain) UIImageView *messageImg;

@property(nonatomic, retain) IBOutlet  UIButton * DoButton;
@property(nonatomic, retain) IBOutlet  UIButton * ReButton;
@property(nonatomic, retain) IBOutlet  UIButton * MiButton;
@property(nonatomic, retain) IBOutlet UIButton * FaButton;
@property(nonatomic, retain) IBOutlet UIButton * SolButton;
@property(nonatomic, retain) IBOutlet UIButton * LaButton;
@property(nonatomic, retain) IBOutlet UIButton * SiButton;

@property(nonatomic, retain) IBOutlet  UITextField *signature;
@property(nonatomic, retain) IBOutlet  UITextField *key;
@property(nonatomic, retain) IBOutlet  UITextView *message;
@property(nonatomic, retain) IBOutlet  UILabel *verLabel;


-(IBAction)Verify;
- (IBAction) tapBackground: (id) sender;


-(IBAction)createKEY;
-(IBAction)doAct;
-(IBAction)reAct;
-(IBAction)miAct;
-(IBAction)faAct;
-(IBAction)solAct;
-(IBAction)laAct;
-(IBAction)siAct;
-(IBAction)DONE;

@end

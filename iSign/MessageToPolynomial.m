//
//  MessageToPolynomial.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-24.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MessageToPolynomial.h"


@implementation MessageToPolynomial

-(polinomio)convertMessage:(NSString *)message{
     PolynomialClass *polyArithm = [[PolynomialClass alloc]init];
    NSMutableArray *charzAsInts = [[NSMutableArray alloc]init];
    NSArray *charArray = [[NSArray alloc]initWithObjects:@" ",@"A",@"B",@"C",@"D",@"E",@"F"
    ,@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"`",@"~",@".",@"#",@"$",@"%",@"^",@"&",@"*",@"(",@")",@"-",@"_",@"=",@"+",@"[",@"]",@"{",@"}",@",",@";",@":",@"?",@"/",@"<",@">", nil];
    for (int i = 0; i<[message length]; i++) {
        
    NSString *partChar = [NSString stringWithFormat:@"%c",[message characterAtIndex:i] ];
    NSString *CharSt = [NSString stringWithFormat:@"%i", [charArray indexOfObject:partChar]];
    [charzAsInts addObject:CharSt];
  //  NSLog(@"%@",partChar);
    //    NSLog(@"%@",CharSt);
    }
    polinomio partial = 1;
    for (int j = 0; j< [charzAsInts count]; j++) {
       
        polinomio monomioMod = [[charzAsInts objectAtIndex:j]intValue];
        partial = partial + monomioMod;
    
     }
    polinomio final = [polyArithm suma:partial :[message length]] ^ 1111;
    return (final);
}

-(NSMutableArray *)fileToOperator:(NSString *)message:(int)dimension{
    NSMutableArray *operator = [[NSMutableArray alloc]initWithCapacity:dimension];
    NSMutableString * paddedM = [[NSMutableString alloc]initWithString:message];
    char im[[message length]];
    char dest[sizeof(im)];
    
   ///////////////////////////////////////////////////////
    if ([message length] == dimension) {
        
        
        for (int l = 0; l<[message length]; l++) {
            char part = [message characterAtIndex:l];
            im[l] = part;
        }
        

    
    for(int j=0;j<[message length];j++)
    {
        memcpy(dest,im+j,4);
       // printf("%c, %u, %i\n",dest[0],dest[0],dest[0]);
        NSString *par = [NSString stringWithFormat:@"%u", dest[0]];
        [operator addObject:par];
    }
        return (operator); }
    
    
    ////////////////////////////////////////////////
    if ([message length] < dimension) {
        
        
        for (int l = 0; l<[message length]; l++) {
            char part = [message characterAtIndex:l];
            im[l] = part;
        }
        
        
        
        for(int j=0;j<[message length];j++)
        {
            memcpy(dest,im+j,4);
            // printf("%c, %u, %i\n",dest[0],dest[0],dest[0]);
            NSString *par = [NSString stringWithFormat:@"%u", dest[0]];
            [operator addObject:par];
        }
        return (operator); }

        
  /*      int dif = dimension - [message length];
       // NSString *partial;
        for (int i = 0; i<dif; i++) {
            
           [paddedM appendString:@"@"];
           // paddedM = partial;
            
        }
        
        for (int l = 0; l<[paddedM length]; l++) {
            char part = [paddedM characterAtIndex:l];
            im[l] = part;
        }

        NSLog(@"%i, %@",dif, paddedM);
        
        for(int j=0;j<[paddedM length];j++)
        {
            memcpy(dest,im+j,1);
            // printf("%c, %u, %i\n",dest[0],dest[0],dest[0]);
            NSString *par = [NSString stringWithFormat:@"%u", dest[0]];
            [operator addObject:par];
        }
        return (operator);
   
    } */
//////////////////////////////////////////////////////////////////////////////////////////////
       float length = (float)[message length];
    float dim = (float)dimension;
    
    float div = length/dim;
    float multF = floor(div);
    int mult = (int) multF +1;
    int resAt = length - (dimension * (mult-1));
    int residuoF = dimension - resAt;
    
    NSMutableArray *partial = [[NSMutableArray alloc]init];
    NSMutableArray *OPpartial = [[NSMutableArray alloc]init];
    

    
      if ([message length] > dimension) {
        
        
        //  NSLog(@"%f,%f,%i,%i, %i", div, multF, mult, resAt, residuoF);
          
          for (int i = 0; i<residuoF; i++) {
             
              int iMod = i % 5;
              if (iMod == 0)[paddedM appendString:@"@"] ;
              if (iMod == 1)[paddedM appendString:@"#"];
              if (iMod == 2)[paddedM appendString:@"*"];
              if (iMod == 3)[paddedM appendString:@"$"] ;
              if (iMod == 4)[paddedM appendString:@"^"] ;
             
              
              
              // paddedM = partial;
              
          }
          for (int l = 0; l<[paddedM length]; l++) {
              char part = [paddedM characterAtIndex:l];
              im[l] = part;
          }
          
        //  NSLog(@"%i, %@",residuoF, paddedM);

      }
    
    for(int j=0;j<[paddedM length];j++)
    {
        memcpy(dest,im+j,4);
        NSString *par = [NSString stringWithFormat:@"%u", dest[0]];
        [operator addObject:par];
    }
    
    for (int m = 0; m < dimension; m++) {
        
      
        
        for (int l = m * mult; l < ((m+1) * mult); l++) {
            
       
            NSString *part = [[NSString alloc]initWithString:[operator objectAtIndex:l]];
            [partial addObject:part];
           // NSLog(@"%@", partial);
            
        }
       // NSLog(@"%@", partial);
        polinomio partialPOL = 0;
        for (int k = 0; k< mult; k++) {
            
            //
            polinomio monomio = [[partial objectAtIndex:k]intValue];
            partialPOL = partialPOL^monomio;
        }
        NSString *prt = [NSString stringWithFormat:@"%ld", partialPOL];
        [OPpartial addObject:prt];

        
       // [OPpartial addObject:partial];
     
        
      //  NSLog(@"%@", [OPpartial objectAtIndex:m]);
        //NSLog(@"%@", partial);
        [partial removeAllObjects];
       }
    
   //  NSLog(@"%@", OPpartial);
    
  
  /*  for (int s = 0; s < [OPpartial count]; s++) {
        NSMutableArray *pt = [[NSMutableArray alloc]initWithArray:[OPpartial objectAtIndex:s]];
        NSLog(@"%@", pt);
        polinomio partialPOL = 0;
        for (int k = 0; k< mult; k++) {
            
          //
           polinomio monomio = [[pt objectAtIndex:k]intValue];
            partialPOL = partialPOL^monomio;
        }
        NSString *prt = [NSString stringWithFormat:@"%ld", partialPOL];
        [operator addObject:prt];
        [pt release];
        
    } 
    
    [partial release];
    [OPpartial release]; */
  

    
       
    return (OPpartial);

}

@end

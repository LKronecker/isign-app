//
//  PolynomialRingOverGL(2^n).m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-09.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PolynomialRingOverGL(2^n).h"

@implementation PolynomialRingOverGL_2_n_

-(NSArray *) generateRing:(int)k{
  //  NSMutableArray *elemantsAsInts = [[NSMutableArray alloc]init];
   // NSMutableArray *partialArray = [[NSMutableArray alloc]init];
    
    NSSet *combinations = [[NSSet alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7", nil];
    NSSet *combinationSet = [combinations variationsWithRepetitionsOfSize:k];
    NSArray *var = [combinationSet allObjects];
    ringElements = var;
    dimension = k;
    order = [var count];
    
  //  int s = [var count];
  //  NSLog(@"%@", [var objectAtIndex:0]);
   // NSLog(@"%i",s);
    return (var);
}

-(NSString *) displayElment: (int)e{
      NSMutableString* partialPoly = [NSMutableString string];
   // NSMutableArray *partialPolyArray = [[NSMutableArray alloc]init];
    for (int j=6; j<dimension*7; j=j+7) {
        NSString *poly = [NSString stringWithFormat:@"%@",[ringElements objectAtIndex:e]];
        // NSString *poly = [NSString stringWithFormat:@"%@",[var objectAtIndex:i]];
        char firstVal = [poly characterAtIndex:j];
        NSString *intPoly = [NSString stringWithFormat:@"%c", firstVal];
        

        [partialPoly appendString:intPoly];
        
    }
   
     NSMutableString* partialPolyString = [NSMutableString string];
    for (int i = 0; i<[partialPoly length]; i++) {
        char firstVal = [partialPoly characterAtIndex:i];
        
        
        NSString *partialMonomial = [NSString stringWithFormat:@" %cY^%i +", firstVal, i];
        [partialPolyString appendString:partialMonomial];
        
    }
 return (partialPolyString);

}

-(polinomio) evaluateElement:(polinomio)p :(int)element{
    NSMutableString* partialPoly = [NSMutableString string];
    for (int j=6; j<dimension*7; j=j+7) {
        NSString *poly = [NSString stringWithFormat:@"%@",[ringElements objectAtIndex:element]];
        // NSString *poly = [NSString stringWithFormat:@"%@",[var objectAtIndex:i]];
        char firstVal = [poly characterAtIndex:j];
        NSString *intPoly = [NSString stringWithFormat:@"%c", firstVal];
        
        [partialPoly appendString:intPoly];
        
    }
//    NSLog(@"%@", partialPoly);

    NSMutableArray *Monomials = [[NSMutableArray alloc]init];
    FiniteFIeldsBase2 *evaluarBase2 = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *prod = [[PolynomialClass alloc]init];

    for (int i = 0; i<dimension; i++) {
                polinomio monomioElev = [evaluarBase2 elevarGFbase2:p :i];

        char val = [partialPoly characterAtIndex:i];
        NSString *coeffChar = [NSString stringWithFormat:@"%c",val];
        polinomio coeff = [coeffChar intValue];
        
        polinomio MonomioI = [prod multiplicacion:monomioElev :coeff];
            NSLog(@"%ld", MonomioI);
        polinomio MonomioIMod = [prod residuo:MonomioI :11];
        NSString *monomial = [NSString stringWithFormat:@"%ld", MonomioIMod];
       
        [Monomials insertObject:monomial atIndex:0];
        
    }
     NSLog(@"%@", Monomials);
    polinomio partial = 0;
    for (int j = 0; j< [Monomials count]; j++) {
        polinomio monomioMod = [[Monomials objectAtIndex:j]intValue];
        partial = partial^monomioMod;
    }
  //  NSLog(@"%ld", partial);
    return (partial);
    [evaluarBase2 release];
    [Monomials release];
    [prod release];
}

-(polinomio) evluatePolyinOperator:(polinomio)elem:(NSString *)oper: (int)exp{
    NSMutableArray *monomialArray = [[NSMutableArray alloc]init];
   // NSString *operST = [NSString stringWithFormat:@"%ld",oper];
    FiniteFIeldsBase2 *evaluarBase2 = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *polyArithmetics = [[PolynomialClass alloc]init];
//    Key_Expanssion *keyExpand = [[Key_Expanssion alloc]init];
    MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];

    NSArray *irredArray= [evaluarBase2 irreductiblePolynomials:exp];
    polinomio irred = [[irredArray objectAtIndex:0] intValue];
    
    monomialArray = [convertFile fileToOperator:oper :1];

    
  //  monomialArray = [keyExpand ExpandKey:oper :1];
    
    /* if (exp <20){ monomialArray = [keyExpand ExpandKey:oper :6];}
    if (exp <17){ monomialArray = [keyExpand ExpandKey:oper :6];}
    if (exp <14){ monomialArray = [keyExpand ExpandKey:oper :5];}
    if (exp <10){ monomialArray = [keyExpand ExpandKey:oper :4];}
    if (exp <7){ monomialArray = [keyExpand ExpandKey:oper :3];} */
    
    
   // NSLog(@"%@", monomialArray);
    
    for (int i = 0; i<[monomialArray count]; i++) {
        polinomio coef = [[monomialArray objectAtIndex:i]intValue];
        polinomio coefMOD = [polyArithmetics residuo:coef :irred];
        NSString *coefMODstring = [NSString stringWithFormat:@"%ld", coefMOD];
        
        [monomialArray replaceObjectAtIndex:i withObject:coefMODstring];
    }
     
    for (int j = 0; j<[monomialArray count]; j++) {
         polinomio monomioElev = [evaluarBase2 elevarGFbase2:elem :j+1];
        polinomio coeff = [[monomialArray objectAtIndex:j]intValue];
        polinomio MonomioI = [polyArithmetics multiplicacion:monomioElev :coeff];
        polinomio MonomioIMod = [polyArithmetics residuo:MonomioI :irred];
        NSString *monomial = [NSString stringWithFormat:@"%ld", MonomioIMod];
        [monomialArray replaceObjectAtIndex:j withObject:monomial];
    }
   //  NSLog(@"%@", monomialArray);
    polinomio partial = 0;
    for (int k = 0; k< [monomialArray count]; k++) {
        polinomio monomioMod = [[monomialArray objectAtIndex:k]intValue];
        partial = partial^monomioMod;
    }
    
    return (partial);
    [evaluarBase2 release];
    [monomialArray release];
    [polyArithmetics release];

}

-(polinomio) ReedSolomonEval:(polinomio)elem:(NSString *)oper:(int)exp{
    NSMutableArray *monomialArray = [[NSMutableArray alloc]init];
 //   NSString *operST = [NSString stringWithFormat:@"%lld",oper];
    FiniteFIeldsBase2 *evaluarBase2 = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *polyArithmetics = [[PolynomialClass alloc]init];
    
  //  Key_Expanssion *keyExpand = [[Key_Expanssion alloc]init];
    MessageToPolynomial *convertFile = [[MessageToPolynomial alloc]init];
    
    NSArray *irredArray= [evaluarBase2 irreductiblePolynomials:exp];
    polinomio irred = [[irredArray objectAtIndex:0] intValue];
    
  //  monomialArray = [keyExpand ExpandKey:oper :1];
    
   // if (exp <20){ monomialArray = [keyExpand ExpandKey:oper :6];}
    monomialArray = [convertFile fileToOperator:oper :65536];
   
    /*if (exp <14){ monomialArray = [keyExpand ExpandKey:oper :5];}
    if (exp <10){ monomialArray = [keyExpand ExpandKey:oper :4];}
    if (exp <7){ monomialArray = [keyExpand ExpandKey:oper :3];} */
    
    
  //  NSLog(@"%@", monomialArray);
    
    for (int i = 0; i<[monomialArray count]; i++) {
        polinomio coef = [[monomialArray objectAtIndex:i]intValue];
        polinomio coefMOD = [polyArithmetics residuo:coef :irred];
        NSString *coefMODstring = [NSString stringWithFormat:@"%ld", coefMOD];
        
        [monomialArray replaceObjectAtIndex:i withObject:coefMODstring];
    }
   // NSLog(@"%@", monomialArray);
    
    
    for (int j = 0; j<[monomialArray count]; j++) {
        polinomio monomioElev = [evaluarBase2 elevarGFbase2:elem :j+1];
        polinomio coeff = [[monomialArray objectAtIndex:j]intValue];
        polinomio MonomioI = [polyArithmetics multiplicacion:monomioElev :coeff];
        polinomio MonomioIMod = [polyArithmetics residuo:MonomioI :irred];
        NSString *monomial = [NSString stringWithFormat:@"%li", MonomioIMod];
        [monomialArray replaceObjectAtIndex:j withObject:monomial];
        
     //   NSLog(@"%li,%li,%li,%li,%@", monomioElev, coeff, MonomioI, MonomioIMod, monomial);
    }
   // NSLog(@"%@", monomialArray);
    polinomio partial = 0;
    for (int k = 0; k< [monomialArray count]; k++) {
        polinomio monomioMod = [[monomialArray objectAtIndex:k]intValue];
        partial = partial^monomioMod;
    }
    
    return (partial);
    [evaluarBase2 release];
    [monomialArray release];
    [polyArithmetics release];
    
}

  /*  for (int i = 0; i<[var count]; i++) {
        
       
        //NSLog(@"%@", poly);

        for (int j=6; j<23; j=j+7) {
             NSString *poly = [NSString stringWithFormat:@"%@",[var objectAtIndex:i]];
           // NSString *poly = [NSString stringWithFormat:@"%@",[var objectAtIndex:i]];
             char firstVal = [poly characterAtIndex:j];
            NSString *intPoly = [NSString stringWithFormat:@"%c", firstVal];
                      [partialArray addObject:intPoly];
          //  NSLog(@"%@", partialArray);
        
        }
       
    }
elemantsAsInts = partialArray;
    NSMutableArray *varWrepetitions = [[NSMutableArray alloc]init];
      NSMutableString* partialPoly = [NSMutableString string];
    
    for (int i = 0; i<k; i++) {
    
    NSString *val = [elemantsAsInts objectAtIndex:i];
    [partialPoly appendString:val];
       
    }
     [varWrepetitions insertObject:partialPoly atIndex:0];
    return (varWrepetitions);

}*/

@end

//
//  FirstViewController.h
//  iSign
//
//  Created by Leopoldo G Vargas on 1/27/2014.
//  Copyright (c) 2014 HaiKu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirmasDigitalesUH.h"
#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"
#import "LFSR.h"
#import "lfsrCipher.h"
#import <AudioToolbox/AudioToolbox.h>

@interface FirstViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextViewDelegate>{
    
    SystemSoundID mySound;
    
    int keySelectionCount;
    
    UIImageView *messageImg;
    
    polinomio finalKey;
    bool keyVIEW;
    bool messgView;
    
    UIView *keyView;
    NSString *keyString;
    NSMutableArray *keyArray;
    int keyCount;
    
    UIButton * DoButton;
    UIButton * ReButton;
    UIButton * MiButton;
    UIButton * FaButton;
    UIButton * SolButton;
    UIButton * LaButton;
    UIButton * SiButton;
    
    UIButton *signButton;
    
    UITextField *key;
    UITextView *message;
    UILabel *signLabel;
    UILabel *keyLabel;
    
    IBOutlet UIImageView *image;
	UIImagePickerController *imgPicker;
    NSDictionary *imageDictinary;
    
    NSData *imageData;
    NSString *imageName;
    NSURL *imageURL;
    
    NSString *CHYPHERState;
    NSString *ORIGINstate;
    NSString *Signature;
}
@property(nonatomic, retain)  NSString *keyString;
@property(nonatomic, retain)  NSMutableArray *keyArray;
@property(nonatomic, retain) IBOutlet   UIView *keyView;
@property(nonatomic, retain) IBOutlet  UILabel *keyLabel;

@property(nonatomic, retain)    UIImageView *messageImg;

@property(nonatomic, retain) IBOutlet  UIButton * DoButton;
@property(nonatomic, retain) IBOutlet  UIButton * ReButton;
@property(nonatomic, retain) IBOutlet  UIButton * MiButton;
@property(nonatomic, retain) IBOutlet UIButton * FaButton;
@property(nonatomic, retain) IBOutlet UIButton * SolButton;
@property(nonatomic, retain) IBOutlet UIButton * LaButton;
@property(nonatomic, retain) IBOutlet UIButton * SiButton;

@property(nonatomic, retain) IBOutlet   UIButton *signButton;

@property(nonatomic, retain) IBOutlet  UITextField *key;
@property(nonatomic, retain) IBOutlet  UITextView *message;
@property(nonatomic, retain) IBOutlet  UILabel *signLabel;
@property (nonatomic, retain) UIImagePickerController *imgPicker;
@property (nonatomic, retain) NSDictionary *imageDictinary;
@property (nonatomic, retain)   NSData *imageData;

@property (nonatomic, retain) NSString *imageName;

@property (nonatomic, retain) NSString *ChipherState;
@property (nonatomic, retain) NSString *ORIGINstate;
@property (nonatomic, retain) NSString *Signature;

@property (nonatomic, retain) NSURL *imageURL;


-(IBAction)Sign;
-(IBAction)SignImage;
- (IBAction) tapBackground: (id) sender;
- (IBAction)grabImage;
-(IBAction)LFSRcipher;

-(IBAction)createKEY;
-(IBAction)doAct;
-(IBAction)reAct;
-(IBAction)miAct;
-(IBAction)faAct;
-(IBAction)solAct;
-(IBAction)laAct;
-(IBAction)siAct;
-(IBAction)DONE;
-(void)alert;


@end

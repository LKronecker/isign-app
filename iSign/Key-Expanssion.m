//
//  Key-Expanssion.m
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-31.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Key-Expanssion.h"

@implementation Key_Expanssion

-(NSMutableArray *) ExpandKey:(long)seed: (int)length{
    
    NSString *seedST = [NSString stringWithFormat:@"%ld", seed];
    NSMutableArray *seedArray = [[NSMutableArray alloc]init];
    FiniteFIeldsBase2 *fieldArithm = [[FiniteFIeldsBase2 alloc]init];
    PolynomialClass *poliArithm = [[PolynomialClass alloc]init];
    if (length == 1){
    [seedArray addObject:seedST];
        return (seedArray);
    }
    else{
    if ([seedST length] & 1) {
       // NSLog(@"impar");
        for (int i = 0; i < [seedST length]; i=i+1) {
            char seed0 = [seedST characterAtIndex:i];
            NSString *charSeed = [NSString stringWithFormat:@"%c",seed0 ];
            [seedArray addObject:charSeed];}
    } else {
      //  NSLog(@"par");
    for (int i = 0; i < [seedST length]; i=i+2) {
        char seed0 = [seedST characterAtIndex:i];
        char seed1 = [seedST characterAtIndex:i+1];
        NSString *charSeed = [NSString stringWithFormat:@"%c%c",seed0, seed1 ];
        [seedArray addObject:charSeed];
    }}
    if (length == 2) return (seedArray);
    
    if (length == 3) {
        
    for (int j = 0; j<[seedArray count]; j++) {
        polinomio var1 = [[seedArray objectAtIndex:j]intValue];
        polinomio var3 =[fieldArithm elevarGFbase2:var1 :3];
        polinomio varFinal = var3 ^ var1 ^ 1;
        polinomio varFinalMOD = [poliArithm residuo:varFinal :13];
        NSString *final = [NSString stringWithFormat:@"%ld%ld", var1, varFinalMOD];
        
        [seedArray replaceObjectAtIndex:j withObject:final];
        
    }
    }     
    if (length == 4) {
        
        for (int j = 0; j<[seedArray count]; j++) {
            polinomio var1 = [[seedArray objectAtIndex:j]intValue];
            polinomio var3 =[fieldArithm elevarGFbase2:var1 :3];
            polinomio varFinal = var3 ^ var1 ^ 1;
            polinomio varFinalMOD = [poliArithm residuo:varFinal :13];
            NSString *final = [NSString stringWithFormat:@"%ld%ld", var1, varFinalMOD];
            
            polinomio var12 = [final intValue];
            polinomio var32 =[fieldArithm elevarGFbase2:var12 :3];
            polinomio varFinal2 = var32 ^ var12 ^ 1;
            polinomio varFinalMOD2 = [poliArithm residuo:varFinal2 :13];
            NSString *final2 = [NSString stringWithFormat:@"%@%ld", final, varFinalMOD2];
            
            [seedArray replaceObjectAtIndex:j withObject:final2];
            
        }
    }     
    
    if (length == 5) {
        
        for (int j = 0; j<[seedArray count]; j++) {
            polinomio var1 = [[seedArray objectAtIndex:j]intValue];
            polinomio var3 =[fieldArithm elevarGFbase2:var1 :3];
            polinomio varFinal = var3 ^ var1 ^ 1;
            polinomio varFinalMOD = [poliArithm residuo:varFinal :13];
            NSString *final = [NSString stringWithFormat:@"%ld%ld", var1, varFinalMOD];
            
            polinomio var12 = [final intValue];
            polinomio var32 =[fieldArithm elevarGFbase2:var12 :3];
            polinomio varFinal2 = var32 ^ var12 ^ 1;
            polinomio varFinalMOD2 = [poliArithm residuo:varFinal2 :13];
            NSString *final2 = [NSString stringWithFormat:@"%@%ld", final, varFinalMOD2];
            
            polinomio var13 = [final2 intValue];
            polinomio var33 =[fieldArithm elevarGFbase2:var13 :3];
            polinomio varFinal3 = var33 ^ var13 ^ 1;
            polinomio varFinalMOD3 = [poliArithm residuo:varFinal3 :13];
            NSString *final3 = [NSString stringWithFormat:@"%@%ld", final2, varFinalMOD3];
            
            [seedArray replaceObjectAtIndex:j withObject:final3];
            
        }
    }     
        if (length == 6) {
        
        for (int j = 0; j<[seedArray count]; j++) {
            polinomio var1 = [[seedArray objectAtIndex:j]intValue];
            polinomio var3 =[fieldArithm elevarGFbase2:var1 :3];
            polinomio varFinal = var3 ^ var1 ^ 1;
            polinomio varFinalMOD = [poliArithm residuo:varFinal :13];
            NSString *final = [NSString stringWithFormat:@"%ld%ld", var1, varFinalMOD];
            
            polinomio var12 = [final intValue];
            polinomio var32 =[fieldArithm elevarGFbase2:var12 :3];
            polinomio varFinal2 = var32 ^ var12 ^ 1;
            polinomio varFinalMOD2 = [poliArithm residuo:varFinal2 :13];
            NSString *final2 = [NSString stringWithFormat:@"%@%ld", final, varFinalMOD2];
            
            polinomio var13 = [final2 intValue];
            polinomio var33 =[fieldArithm elevarGFbase2:var13 :3];
            polinomio varFinal3 = var33 ^ var13 ^ 1;
            polinomio varFinalMOD3 = [poliArithm residuo:varFinal3 :13];
            NSString *final3 = [NSString stringWithFormat:@"%@%ld", final2, varFinalMOD3];
            
            polinomio var14 = [final3 intValue];
            polinomio var34 =[fieldArithm elevarGFbase2:var14 :3];
            polinomio varFinal4 = var34 ^ var14 ^ 1;
            polinomio varFinalMOD4 = [poliArithm residuo:varFinal4 :13];
            NSString *final4 = [NSString stringWithFormat:@"%@%ld", final3, varFinalMOD4];
            
            [seedArray replaceObjectAtIndex:j withObject:final4];
            
        }
    }     
    if (length == 7) {
        
        for (int j = 0; j<[seedArray count]; j++) {
            polinomio var1 = [[seedArray objectAtIndex:j]intValue];
            polinomio var3 =[fieldArithm elevarGFbase2:var1 :3];
            polinomio varFinal = var3 ^ var1 ^ 1;
            polinomio varFinalMOD = [poliArithm residuo:varFinal :13];
            NSString *final = [NSString stringWithFormat:@"%ld%ld", var1, varFinalMOD];
            
            polinomio var12 = [final intValue];
            polinomio var32 =[fieldArithm elevarGFbase2:var12 :3];
            polinomio varFinal2 = var32 ^ var12 ^ 1;
            polinomio varFinalMOD2 = [poliArithm residuo:varFinal2 :13];
            NSString *final2 = [NSString stringWithFormat:@"%@%ld", final, varFinalMOD2];
            
            polinomio var13 = [final2 intValue];
            polinomio var33 =[fieldArithm elevarGFbase2:var13 :3];
            polinomio varFinal3 = var33 ^ var13 ^ 1;
            polinomio varFinalMOD3 = [poliArithm residuo:varFinal3 :13];
            NSString *final3 = [NSString stringWithFormat:@"%@%ld", final2, varFinalMOD3];
            
            polinomio var14 = [final3 intValue];
            polinomio var34 =[fieldArithm elevarGFbase2:var14 :3];
            polinomio varFinal4 = var34 ^ var14 ^ 1;
            polinomio varFinalMOD4 = [poliArithm residuo:varFinal4 :13];
            NSString *final4 = [NSString stringWithFormat:@"%@%ld", final3, varFinalMOD4];
            
            polinomio var15 = [final4 intValue];
            polinomio var35 =[fieldArithm elevarGFbase2:var15 :3];
            polinomio varFinal5 = var35 ^ var15 ^ 1;
            polinomio varFinalMOD5 = [poliArithm residuo:varFinal5 :13];
            NSString *final5 = [NSString stringWithFormat:@"%@%ld", final4, varFinalMOD5];
            
            [seedArray replaceObjectAtIndex:j withObject:final5];
            
        }
    }     

    }


    return (seedArray);
    }
@end

//
//  FiniteFIeldsBase2.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-04-26.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PolynomialClass.h"

@interface FiniteFIeldsBase2 : NSObject{
    int base;
    int exponent;
    NSArray *irreducPolyArray;
    NSArray *irredPolIntValArray;
    long order;
    NSArray * elements;
    
}
-(NSArray *) generateField: (int)exp;
-(void) displayField;
-(polinomio)sumaGFbase2:(polinomio)p:(polinomio)q;
-(polinomio)productoGFbase2:(polinomio)p:(polinomio)q;
-(polinomio)elevarGFbase2:(polinomio)p:(int)exp;
-(NSArray *)irreductiblePolynomials: (int)exp;
-(polinomio) compressionEndomorfism:(polinomio)p :(int)newDimension;


@end

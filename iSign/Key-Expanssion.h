//
//  Key-Expanssion.h
//  Polynomials
//
//  Created by Leopoldo G Vargas on 12-05-31.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FiniteFIeldsBase2.h"
#import "PolynomialClass.h"

@interface Key_Expanssion : NSObject

-(NSMutableArray *) ExpandKey:(long)seed: (int)length;

@end

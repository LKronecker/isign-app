//
//  AppDelegate.h
//  iSign
//
//  Created by Leopoldo G Vargas on 1/27/2014.
//  Copyright (c) 2014 HaiKu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
